#include "main.h"

void operatorControl() {
	while (1) {

		// Controller 1/2, Stick L/R, Axis X/Y
		int C1LX = joystickGetAnalog(1, 4);
		int C1LY = joystickGetAnalog(1, 3);
		int C1RX = joystickGetAnalog(1, 1);

		// Y component, X component, Rotation
		/*motorSet(1, C1LY - C1LX - C1RX); //good
		motorSet(2, -C1LY - C1LX + C1RX); //good
		motorSet(3, C1LY - C1LX + C1RX); //good
		motorSet(4, C1LY + C1LX + C1RX);*/
		motorSet(1, (-1 * (C1LY - C1LX - C1RX)));
		motorSet(10, (-1 * (C1LY + C1LX - C1RX)));
		motorSet(3, (-1 * (C1LY - C1LX + C1RX)));
		motorSet(4, (-1 * (C1LY + C1LX + C1RX)));

		//in/outtake
		int inTake = joystickGetDigital(1, 6, JOY_DOWN);
		int outTake = joystickGetDigital(1, 5, JOY_DOWN);

		if(inTake == 1){
			motorSet(5, inTake * 127);
			motorSet(6, inTake * -127);
		}else if(outTake == 1){
			motorSet(5, outTake * -127);
			motorSet(6, outTake * 127);
		}else{
			motorSet(5, 0);
			motorSet(6, 0);
		}


		/*

		 */
		//flywheel

		//int firing = 0;

		int shoot = joystickGetDigital(1, 6, JOY_UP);


		if(shoot == 1){
			motorSet(7, 127);
			motorSet(8, -127);
		}else{
			motorSet(7, 0);
			motorSet(8, 0);
		}

		//extend
		int extend = joystickGetDigital(1, 7, JOY_RIGHT);
		int retract = joystickGetDigital(1, 7, JOY_DOWN);


		if(extend == 1){
			motorSet(9, 127);
		}else if(retract == 1){
			motorSet(9, -127);
		}else{
			motorSet(9, 0);
		}

		//lift
		int lift = joystickGetDigital(1, 8, JOY_LEFT);
		int lower = joystickGetDigital(1, 8, JOY_DOWN);

		if(lift == 1){
			motorSet(2, 127);
		}else if(lower == 1){
			motorSet(2, -127);
		}else{
			motorSet(2,0);
		}

		if(joystickGetDigital(1,8, JOY_UP) == 1){
			autonomous();
		}

		delay(20);
	}
}
